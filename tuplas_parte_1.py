
# TUPLAS INMUTABLES

its_tuple = ("server" , "client" , "response")
print(type(its_tuple))

# INDEX

localitation = its_tuple[:3]
print(localitation)

# its_tuple[0] = "request"
# print(its_tuple)

tuple_convert = list(its_tuple)
print(tuple_convert)

tuple_convert[0] = "request"
print(tuple_convert)

tuple_convert.append("server")
print(tuple_convert)

tuple_convert.remove("server")
print(tuple_convert)

tuple_convert.clear()
print(tuple_convert)

del tuple_convert
print(tuple_convert)

# its_tuple = tuple(tuple_convert)
# print(its_tuple)
