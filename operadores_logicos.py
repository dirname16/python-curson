# VARIABLES

x = 5
y = 10

numeros = ["one" , "two" , "three"]

# OPERADOR AND 

problema = x > 3 and x < 10
print(problema)

# OPERADOR OR

problema = x > 10 or x < 10
print(problema)

# OPERADOR NOT

problema = not(x > 3 and x < 10)
print(problema)

# IS

problema = x is y
print(problema)

# IS NOT

problema = x is not y
print(problema)

# IN

problema = "four" in numeros
print(problema)

# NOT IN

problema = "four" not in numeros
print(problema)