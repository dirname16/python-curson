# print(Este es un comentario)

Un poco sobre cada nuevo función y para qué sirve
Una explicación de para qué sirve una variable o un conjunto de variables
Explicando por qué hiciste algo de cierta manera (si no es obvio)
Resaltando teclas y partes importantes de tu código
Proporcionar advertencias

Mantenga sus comentarios concisos y no más de lo necesario: ¡respete el tiempo de su lector! [19659024] Evite los 
comentarios que afirman lo obvio; no más allá del comentario
¡No te limites a explicar qué hace algo: explica por qué lo pones allí y por qué es importante
y amigable! No uses comentarios para avergonzar a otros programadores. Esta es una forma rápida de convertirse en la 
persona menos popular en su equipo.


Los comentarios en el encabezado también se pueden usar como un lugar para colocar un aviso de copyright o para declarar 
la autoría del código. A algunas personas les gusta usar ASCII exagerado para dar títulos llamativos a sus códigos.


Estructurar lógicamente su código
Usar nombres inteligentes para variables y funciones, junto con una convención de nomenclatura coherente
Usar nuevas líneas y sangrías correctamente (por suerte, Python nos obliga a hacer lo último)

# ESTE ES UN COMENTARIO
'''
    ESTE ES UN COMENTARIO MULTILINEA
'''

"""
    ESTE ES UN COMENTARIO MULTILINEA
"""
