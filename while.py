# AXULIAR
i = 0

numbers = ["one" , "two" , "thre" , "four" , "five" , "six"]

# BUCLE WHILE SIMPLE

while i < 11 :
    print(i)
    i += 1

# CONDICIONES DENTRO DE UN BUCLE WHILE

while i < 11 :
    print(i)

    if i == 5 :
        print("Funcionando")
    elif i == 6 :
        print("Este Si Funciona")
    else :
        print("No Funciona")

    i += 1

# BREAK

while i < 11 :
    print(i)

    if i == 5 :
        print("Hasta Aqui")
        break

    i += 1

# CONTINUE

while i < 20 :
    print(i)

    if i == 5 :
        print("Continuando")
        i += 5
        continue


    i += 1

# RECORRER UNA LISTA

while i < len(numbers) :
    print(numbers[i])
    i += 1  