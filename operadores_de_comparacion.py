# OPERADORES DE COMPARACION


"""
    VARIABLES 
"""
x = 19
y = 20

# OPERADOR DE COMPARACION IGUAL

resultado = x == y
print("El resultado del operador de igual es :" , resultado)

# OPERADOR DE DESIGUALDAD

resultado = x != y
print("El resultado del operador de desigualdad es :" , resultado)

# OPERADOR DE MAYOR

resultado = x > y
print("El resultado del operador de mayor es :" , resultado)

# OPERADOR DE MENOR

resultado = x < y
print("El resultado del operador de menor es :" , resultado)

# OPERADOR DE MAYOR IGUAL

resultado = x >= y 
print("El resultado del operador de mayor igual es :" , resultado)

# OPERADOR DE MENOR IGUAL

resultado = x <= y
print("El resultado del operador de menor igual es :" , resultado)