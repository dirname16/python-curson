# SET IN PYTHON

numbersint = {1 , 2 , 3}
numbersstr = {"1" , "2" , "3" , "3"}

# NO INDEX
# NO ORDER
# NO DUPLICATE
# NO CHANGE

print(numbersint)
print(numbersstr)

# 1 IN NUMBERSSTR

print("1" in numbersstr)

# ADD ELEMENT A SET

numbersstr.add("4")

print(numbersstr)

# UPDATE

numbersstr.update(numbersint)
print(numbersstr)