its_tuple = ("server" , "client" , "response")

its_tuple += ("new_value" ,)

print(its_tuple)

tuple1 = ("apple" , "banana" , "cherry" , "response" , "response" , "client" , "server")
tuple2 = its_tuple

response = tuple1 + tuple2 
print(response)

new_tuple = tuple1 * 2
print(new_tuple)

# DESEMPACAMIENTO

tuple_des = tuple1
(frut1 , *frut2 , frut3) = tuple_des
print(frut2)

# METHODS

print(tuple_des.count("response"))
print(tuple_des.index("response"))