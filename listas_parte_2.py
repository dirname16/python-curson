# LISTA
the_list = ["response" , "client" , "servidor" , False , 1]


# AGREGAR

the_list.append(2)
print(the_list)

# INSERT 

the_list.insert( 1 , "save")
print(the_list)

# REMOVER

the_list.remove("response")
print(the_list)

# ELIMININA EL ULTIMA VALOR DE UNA LISTA
the_list.pop(1)
print(the_list)

# ELIMA CUALQUIER ELEMENTO

del the_list[4]
print(the_list)

# LIMPIRAMOS LA LISTA 
the_list.clear()
print(the_list)

# ELIMINAR CORCHETES

del the_list
print(the_list)