# UNA LISTA = CONTENEDOR

the_list = ["manzana" , "pera" , "fresa" , True , 16]

# DEVULUCION ALL
print(the_list)

# INDEXE

print(the_list[1])
print(the_list[-1])
print(the_list[1:4])
print(the_list[:3])
print(the_list[3:])

# LONGITUD

print(len(the_list))

# EXTENDER

two_list = [2 , 3 , 4 , 100]

the_list.extend(two_list)
print(the_list)