"""
    LA VARIABLE NUMERO ES LLAMADA POR EL METHODO PRINT EN DONDE USAMOS TYPE DONDE VAMOS A COMPROBAR QUE ES DE TIPO INT NUESTRA VARIABLE
"""
numero = 10

print(type(numero))

"""
    LA VARIABLE FLOTANTE ES LLAMADA POR EL METHODO PRINT EN DONDE USAMOS TYPE DONDE VAMOS A COMPROBAR QUE ES DE TIPO FLOAT NUESTRA VARIABLE
"""

flotante = 10.2
print(type(flotante))

"""
    LA VARIABLE TEXT ES LLAMADA POR EL METHODO PRINT EN DONDE USAMOS TYPE DONDE VAMOS A COMPROBAR QUE ES DE TIPO STR NUESTRA VARIABLE
"""

text = "Python"
print(type(text))

"""
    VAMOS A COMPROBAR LAS VARIABLES SI SON DE TIPO BOOLIANO 
"""

boo = True
print(type(boo))
boo = False
print(type(boo))

print(type(10 + 12.5))

print(float(numero))
print(int(flotante))

"""
    VAMOS A SUMAR LAS VARIABLES TEXT + STR(FLOTANTE) Y COMPROBAR EL TIPO DE DATO
"""

rest = text + str(flotante) 
print(type(rest))

"""
    AQUI VAMOS A CONVERTIR NUESTRA VARIABLE CONVERT_STRING EN UN STR DESPUES PASAREMOS A COMPROBAR SI ES DE TIPO STR
"""

convert_string = str(numero)
print(convert_string)
print(type(convert_string))