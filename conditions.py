# CONDITIONALS

# a > b
# a < b
# a >= b
# a <= b
# a != b

a = 100
b = 200

# SENTENCIA IF

if a < b :
    print(True)

# SENTENCIA ELSE

if a == b :
    print("A == B")
else :
    print("A is not B")

# SENTENCIA ELIF

if a == b :
    print("A == B")
elif a < b :
    print("A es menor que B")
else :
    print("Not results")

# PASS

if a :
    pass

# OPERADORES TERNARIOS

if a < b: print("Es Verdad")
print("Es Verdad") if a > b else print("No es Verdad")

# ANIDACIONES

if a < b :
    if a != b :
        print("A is not B")
        if a != b :
            print("A is not B")
    if a != b :
        print("A is not B")
    if a != b :
        print("A is not B")
    if a != b :
        print("A is not B")

# DATOS LOGICOS

if a < b and b > a :
    print("Verdadero")

if a < b or a > b :
    print("Verdadero")

if a is not b :
    print("A is not B")