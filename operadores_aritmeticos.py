# VARIABLE X
x = 10

# VARIABLE
y = 5

# SUMA

resultado = x + y
print('el resultado de la suma es :' , str(resultado))

# RESTA

resultado = x - y
print('el resultado de la resta es :' , resultado)

# MULTIPLICACION

resultado = x * y
print('el resultado de la multiplicacion es :' , resultado)

# DIVISION

resultado = x / y
print('el resultado de la division es :' , resultado)

# POTENCIACION

resultado = x ** y
print('el resultado de la potenciacion es :' , resultado)

# FLOOR DIVISION

resultado = x // y
print('el resultado del floor division es :' , resultado)

# MODULO

resultado = x % y
print('el resultado del modulo es :' , resultado)