# SETS

set_1 = {"google" , "microsoft" , "facebook"}
set_2 = {"server" , "request" , "client" , "google"}

# METHODS

# UNION

set_3 = set_1.union(set_2)
print(set_3)

# INTERSECTION UPDATE

set_1.intersection_update(set_2)
print(set_1)

# INTERSECTION

set_4 = set_1.intersection(set_2)
print(set_4)

# SYMMETRIC DIFFERENCE UPDATE

set_1.symmetric_difference_update(set_2)
print(set_1)

# SYMMETRIC DIFFERENCE

set_5 = set_1.symmetric_difference(set_2)
print(set_5)

# REMOVE

set_1.remove("microsoft")
print(set_1)

# DISCARD 

set_1.discard("facebook")
print(set_1)

# POP

set_1.pop()
print(set_1)

# CLEAR

set_2.clear()
print(set_2)

# DELETE

del set_1
print(set_1)