# CREACION DE VARIABLES

dictionary = {"one" : 1 , "two" : 2 , "three" : 3}
listen = ["request" , "response" , "server" , "hosting"]

# FOR

for x in listen : 
    print(x)

# IMPRESION LETRA POR LETRA

for x in "request" :
    print(x)

# PASS

for x in ["e21eda"] :
    # AQUI SE VA A HACER ALGO
    pass

# BREAK

for x in listen :
    print(x)
    if x == "server" :
        break

for x in listen :
    if x == "server" :
        break

    print("--------" , x)

# FOR CON DICTIONARY

print("------------------------")
for x in dictionary.keys() :
    print(dictionary.get(x))

# RANGE

print("------------------------")
for x in range(11) :
    print(x)

print("------------------------")
for x in range(2 , 11) :
    print(x)

print("------------------------")
for x in range(2 , 11 , 2) :
    print(x)
 
# FOR ANIDADOS

print("------------------------")
for x in dictionary : 
    for y in listen :
        print(x , y)

