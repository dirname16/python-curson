# OPERADORES DE ASIGNACION

x = 20

# OPERADOR DE ASIGNACION DE SUMA

x += 1

print('el resultado del operador de asignacion de suma es :' , x)

# OPERADOR DE ASIGNACION DE RESTA

x -= 1

print('el resultado del operador de asignacion de resta es :' , x)

x *= 1

# OPERADOR DE ASIGNACION DE MULTIPLICACION

print('el resultado del operador de asignacion de multiplicacion es :' , x)

x **= 1

# OPERADOR DE ASIGNACION DE EXPONENCIACION

print('el resultado del operador de asignacion de exponenciacion es :' , x)

# OPERADOR DE ASIGNACION DE DIVISION

x /= 1

print('el resultado del operador de asignacion de division es :' , x)

# OPERADOR DE ASIGNACION DE FLOOR DIVISION

x //= 1

print('el resultado del operador de asignacion de floor division es :' , x)

# OPERADOR DE ASIGNACION DE MODULO

x %= 1

print('el resultado del operador de asignacion de modulo es :' , x)