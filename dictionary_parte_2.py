dictionary = {
    "color" : "#fff" ,
    "background" : "#000" ,
    "python" : "django"
}

print(dictionary)

# OBTENIENDO CLAVES

dictionary_keys = dictionary.keys()
print(dictionary_keys)

# ACTUALIZAR NUESTRO DICTIONARY

dictionary.update({"color" : "#000"})
print(dictionary)

# COPIAR DICTIONARY

response = dictionary.copy()
print(response)

# ELIMINACION DE UN ITEM

dictionary.pop("color")
print(dictionary)

# ELIMINACION DEL ULTIMO ITEM

dictionary.popitem()
print(dictionary)

# ELIMACION DE ITEMS 

dictionary.clear()
print(dictionary)

# ELIMINAR EL DICTIONARY

del dictionary

print(dictionary)